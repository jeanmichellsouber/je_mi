window.onload = function(){

	var links = document.querySelectorAll('header .navigation nav ul li');

	links[0].children[0].click();

};

function geturl(url, t){

	el = t.parentNode;

	className = 'current';

	var x = document.getElementsByClassName('current');
	
    for(var i = 0, length = x.length; i < length; i++) {
  		x[i].classList.remove(className);
    }

	if (el.classList){
		el.classList.add(className);
	}

	var request = new XMLHttpRequest();
	request.open('GET', url, true);

	request.onload = function(result) {
	  if (request.status >= 200 && request.status < 400) {
	    document.getElementById("contentloader").innerHTML = request.response;
	    var resp = request.responseText;
	  } else {
	    document.getElementById("contentloader").innerHTML = "The content for this page couldn't be loaded";
	  }
	};

	request.onerror = function() {
	  document.getElementById("contentloader").innerHTML = "Error loading this page";
	};

	request.send();

}

function editContentBaloon(t){
	var li = document.getElementsByTagName('li');

    for(var i = 0, length = li.length; i < length; i++) {
  		li[i].classList.remove('active');
    }

	t.parentNode.classList.add('active');
}

function changeValues(t, bindingField){

	var value = t.parentNode.previousElementSibling.value;
    var elements = document.getElementsByClassName(bindingField);

    if(value){
	    for(var i = 0, length = elements.length; i < length; i++) {
	  		elements[i].innerHTML = value;
	    }
    }

    // I know it's awful to do this, but Idk a better way in VanillaJS. jQuery would be "parents"
    t.parentNode.parentNode.parentNode.classList.remove('active');

}

function saveFromMobile(t){

	inputs = t.parentNode.parentNode.querySelectorAll('input[name]');
	var personName = [];

	inputs.forEach(function(value){

		if(value.value){

			if(value.name == 'firstname'){
				personName.push(value.value);
			}

			if(value.name == 'lastname'){
				personName.push(value.value);
			}

			var classes = document.getElementsByClassName(value.name);

		    for(var i = 0, length = classes.length; i < length; i++) {
		  		classes[i].innerHTML = value.value;

		    }

		}

	});

	if(personName.length > 0){
		var name = document.getElementsByClassName('name');

	    for(var i = 0, length = name.length; i < length; i++) {
	  		name[i].innerHTML = personName.join(' ');

	    }		
	}

	t.parentNode.parentNode.parentNode.classList.remove('editing');
}